import './App.scss';
import Axios from 'axios';

import { useState } from 'react';

function App() {

  /* input */
  const [pokemonName, setPokemonName] = useState('');
  /*  */
  const [pokemonChosen, setPokemonChosen] = useState(false)
  /*  */
  const [pokemon, setPokemon] = useState({
    name: "",
    number: "",
    species: "",
    image: "",
    hp: "",
    attack: "",
    defense: "",
    speed: "",
    type: "",
  });


  /* si hay info, cambia el setPokemonChosen a true e imprime el resultado */
  const searchPokemon = () => {
    Axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`).then(
      (res) => {
        // console.log(res.data);
        setPokemon({
          name: pokemonName,
          number: res.data.id,
          species: res.data.species.name,
          image: res.data.sprites.front_default,
          hp: res.data.stats[0].base_stat,
          attack: res.data.stats[1].base_stat,
          defense: res.data.stats[2].base_stat,
          speed: res.data.stats[5].base_stat,
					type: res.data.types[0].type.name,
        });
        setPokemonChosen(true);
      }
      );
    };


  return (
    <div className="App">
      <div className="TitleSection">
        <h1 className="TitleSection__title">Pokedex</h1>
        <input
        type="text"
        id='searchInput'
        onChange={(event) => {
          setPokemonName(event.target.value.toLowerCase());
        }}/>
        <div>
	        {pokemonName && <button onClick={searchPokemon} className="button-pushable">
          <span className="button-shadow"></span>
          <span className="button-edge"></span>
          <span className="button-front text">
          Catch it!
          </span>
          </button>}
        </div>
      <div className='DisplaySelection'>
        {!pokemonChosen ? (
          <h1 className='DisplaySelection__help'>Please choose a Pokemon</h1>
        ) : (
          <>
            <h1 className='DisplaySelection__name'>{pokemon.name}</h1>
            <div className='DisplaySelection__imgBox'>
              <img
              src={pokemon.image} alt={pokemon.name}
              className='DisplaySelection__imgBox-image'
              />
            </div>
            <div className='DisplaySelection__info'>
              <h4>Number: #{pokemon.number}</h4>
              <h4>Species: {pokemon.species}</h4>
              <h4>Type: {pokemon.type}</h4>
              <h4>Hp: {pokemon.hp}</h4>
              <h4>Attack: {pokemon.attack}</h4>
              <h4>Defense: {pokemon.defense}</h4>
              <h4>Speed: {pokemon.speed}</h4>
            </div>
          </>
        )}
        </div>
      </div>
    </div>
  );
}

export default App;
